from errbot import BotPlugin, botcmd
import feedparser, re
          
class ForumBot(BotPlugin):
    """An plugin recieving the liveticker from SV Darmstadt 98"""
   
        
    def activate(self):
        """Triggers on plugin activation

        You should delete it if you're not using it to override any default behaviour"""
        super(ForumBot, self).activate()

    def deactivate(self):
        """Triggers on plugin deactivation

        You should delete it if you're not using it to override any default behaviour"""
        super(ForumBot, self).deactivate()
        
    def get_configuration_template(self):
        return {'FORUM_URL': u'www.forum-url.togo','VERIFY_CERTIFICATE':True,'LOGIN':u'Username:Password'}
    
    @botcmd(split_args_with=None)
    def forum_news(self,msg,args):
        """ check forum feed for news """
         
        url = self.config['FORUM_URL']
        
        if not self.config['VERIFY_CERTIFICATE']:
            import ssl
            if hasattr(ssl, '_create_unverified_context'):
                ssl._create_default_https_context = ssl._create_unverified_context
      
        if url != 'www.forum-url.togo':
            
            if self.config['LOGIN'] != 'Username:Password':
                login = re.split(':',self.config['LOGIN'],maxsplit=1)
                
                if len(login) == 2:
                    
                    username = login[0]
                    password = login[1]
                    
                    url = username+':'+password+'@'+url+'?auth.http'
            
            url = 'https://'+url
            
            self.log.debug('Get news from {}'.format(url))        
            
            feed = feedparser.parse(url)
            
            message = ''
            
            self.log.debug('Entries:\n{}'.format(feed))
            
            for entry in feed.entries:
            
                message += u'{} - {} \n'.format(entry.author,entry.title)
            
            if message == '':
                message = 'no news found'
                    
            return message
            
        else:
            return 'configurate this plugin'
